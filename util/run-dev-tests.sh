#!/bin/bash

WORKDIR=${0%/*}
BASEDIR="$WORKDIR/.."
cd "$BASEDIR" || exit

if ! prove 'dev-t'; then
    printf 'Developer tests failed!\n' >&2
    exit 1
fi
