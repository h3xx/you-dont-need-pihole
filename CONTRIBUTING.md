# Contributing Guide

## How to Contribute

Send issues/pull requests to the Codeberg.org project: https://codeberg.org/h3xx/you-dont-need-pihole

You can also email me Dan directly, -h3xx{atty-zat}gmx{dizzy-dot}com-.

## Project Scope

This project and the documentation is only meant to provide the bare essentials
of getting a Pi-less Pi-hole replacement up and running.

## Software Scope

Web interfaces are not going to be supported if it can be helped.

## Documentation Scope

Users of the software are expected to know basic networking and
troubleshooting.

If at all possible. link to better documentation instead of copy-pasting.
