# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed
- Require Perl 5.12

### Fixed
- Fix warning when writing blocklist to stdout.
- Various script clean-ups

## [0.2.0] - 2023-06-19

### Added
- dnsmasq: Add 'bogus-priv', 'domain-needed' options
- dnsmasq: Add IPv6 upstream DNS servers
- Add support for allowlists

### Changed
- Use `::` instead of `::1` as the blocked IPv6 address
- Strip `#` comments, whitespace from \*.domains

### Fixed
- Prevent AdroitAdorKhan-EnergizedProtection updates (current repo has a broken
  list)
- Fix `block.list` permissions on first generation.

### Removed
- Removed in-repo custom ad domains list

## [0.1.0] - 2022-11-12
Initial published version

[Unreleased]: https://codeberg.org/h3xx/you-dont-need-pihole/compare/v0.2.0...HEAD
[0.2.0]: https://codeberg.org/h3xx/you-dont-need-pihole/compare/v0.1.0...v0.2.0
[0.1.0]: https://codeberg.org/h3xx/you-dont-need-pihole/releases/tag/v0.1.0
