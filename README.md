# You Don't Need Pi-hole

**Network-wide DNS blocking without extra hardware.**

This project implements the ad-blocking functionality of a
[Pi-hole](https://pi-hole.net) without needing any extra hardware.

A Pi-hole is a [Raspberry Pi](https://www.raspberrypi.com/) based "black hole"
for Internet advertisements. It works by intercepting and answering DNS queries
for a list of known advertisement servers.

This project instead uses `dnsmasq` running on an existing server to answer DNS
queries.

## How to Install

You'll need `dnsmasq` installed for this.

1. Clone the project:

```sh
git clone --recursive https://codeberg.org/h3xx/you-dont-need-pihole.git /etc/you-dont-need-pihole
```

2. Run `update.sh` to generate the blocklist.

3. Update `dnsmasq.d/01-you-dont-need-pihole.conf`, replacing `/etc/you-dont-need-pihole` with wherever the root
of this project is installed to.

4. Add the configuration directory to `dnsmasq.conf`:

```sh
echo 'conf-dir=/etc/you-dont-need-pihole/dnsmasq.d' >> /etc/dnsmasq.conf
```

You can also copy or symlink `dnsmasq.d/01-you-dont-need-pihole.conf` if you need
the config to live somewhere else.

5. Restart the `dnsmasq` service.

6. Make sure `53/udp` is unfirewalled.

7. Go into your router settings and change the IP addressed provied via DHCP to
   be your server's local IP address. [See this thread for a
   walkthrough.](https://discourse.pi-hole.net/t/how-do-i-configure-my-devices-to-use-pi-hole-as-their-dns-server/245)

8. Recommended: Configure your local machine to use the local `dnsmasq` daemon
   for client DNS queries, i.e. software running on the same server as
   `dnsmasq`. This is a good idea because it may save some network traffic
   depending on how your router works. Worst case scenario, it'll do nothing.

If your OS uses `dhcpcd` for network configuration, you can add this line to your
`/etc/dhcpcd.conf`

```
static domain_name_servers=127.0.0.1
```

## License

Copyright (C) 2021-2022 Dan Church.

License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)

with Commons Clause 1.0 (https://commonsclause.com/).

This is free software: you are free to change and redistribute it.

There is NO WARRANTY, to the extent permitted by law.

You may NOT use this software for commercial purposes.
