###############################################################################
# You Don't Need Pi-hole
# Network-wide DNS blocking without extra hardware.
#
# Project URL: https://codeberg.org/h3xx/you-dont-need-pihole
#
# License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
# with Commons Clause 1.0 (https://commonsclause.com/).
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
# You may NOT use this software for commercial purposes.
###############################################################################

# Add our block lists
addn-hosts=/etc/you-dont-need-pihole/local.list
addn-hosts=/etc/you-dont-need-pihole/custom.list
addn-hosts=/etc/you-dont-need-pihole/block.list

# Never forward addresses in the non-routed address spaces.
bogus-priv

# In-memory cache size.
cache-size=10000

# Never forward plain names (without a dot or domain part)
domain-needed

# Do not load /etc/hosts as a dataset for replies. (By default dnsmasq performs
# an implicit "addn-hosts=/etc/hosts"; this prevents it.)
no-hosts

# Return answers to DNS queries from /etc/hosts.
#localise-queries

# Don't log queries - only startup/shutdown messages. (Un-comment this option
# for debugging.)
#log-queries
# Log to a file:
#log-facility=/var/log/you-dont-need-pihole.log

# Enable asynchronous logging and optionally set the limit on the number of
# lines which will be queued by dnsmasq when writing to the syslog is slow.
#log-async

# Give this TTL (in seconds) for DNS names on our block list.
local-ttl=600

# Do not forward DNS requests to `nameserver`s in /etc/resolv.conf.
no-resolv

# For non-blocked DNS queries, telephone the request thru Google's global DNS
# server.
server=2001:4860:4860::8888
server=2001:4860:4860::8844
server=8.8.8.8
server=8.8.4.4
